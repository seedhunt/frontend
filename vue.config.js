module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    proxy: {
      "/statistics": {
        target: "https://seedhunt.net/statistics",
        changeOrigin: true,
        pathRewrite: {
          "^/statistics": ""
        }
      },
      "/api": {
        target: "https://seedhunt.net/api",
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      }
    }
  }
};
