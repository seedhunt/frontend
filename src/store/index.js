import Vue from "vue";
import Vuex from "vuex";
import filters from "./filters";
import legend from "./legend";
import seeds from "./seeds";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== "production",
  modules: {
    filters,
    legend,
    seeds
  }
});
