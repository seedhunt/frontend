import { biomeLabels } from "@/biomes";

export default {
  data() {
    return {
      propertyLabels: {
        seed: "Seed #",
        spawn_biome: "Spawn Biome",
        village_distance: "Village Distance",
        huts: "# Witch Huts",
        monuments: "# Monuments"
      },
      biomeLabels: biomeLabels(),
      biomeGroupLabels: {
        desert: "Desert",
        forest: "Forest",
        jungle: "Jungle",
        mega_taiga: "Mega Taiga",
        mesa: "Mesa",
        mountain: "Mountain",
        mushroom: "Mushroom",
        ocean: "Ocean",
        plains: "Plains",
        roofed: "Roofed",
        savanna: "Savanna",
        snowy: "Snowy",
        swamp: "Swamp",
        taiga: "Taiga"
      },
      biomeGroupBiomes: {
        ocean: [
          "cold_ocean",
          "deep_cold_ocean",
          "deep_frozen_ocean",
          "deep_lukewarm_ocean",
          "deep_ocean",
          "deep_warm_ocean",
          "frozen_ocean",
          "frozen_river",
          "lukewarm_ocean",
          "ocean",
          "warm_ocean"
        ],
        mesa: [
          "badlands",
          "badlands_plateau",
          "eroded_badlands",
          "modified_badlands_plateau",
          "modified_wooded_badlands_plateau",
          "wooded_badlands_plateau"
        ],
        desert: ["desert", "desert_hills", "desert_lakes"],
        savanna: [
          "savanna",
          "savanna_plateau",
          "shattered_savanna",
          "shattered_savanna_plateau"
        ],
        plains: ["plains", "sunflower_plains"],
        swamp: ["swamp", "swamp_hills"],
        forest: [
          "birch_forest",
          "birch_forest_hills",
          "flower_forest",
          "forest",
          "tall_birch_forest",
          "tall_birch_hills",
          "wooded_hills"
        ],
        roofed: ["dark_forest", "dark_forest_hills"],
        mountain: [
          "gravelly_mountains",
          "modified_gravelly_mountains",
          "mountain_edge",
          "mountains",
          "wooded_mountains"
        ],
        mushroom: ["mushroom_fields", "mushroom_field_shore"],
        jungle: [
          "bamboo_jungle",
          "bamboo_jungle_hills",
          "jungle",
          "jungle_edge",
          "jungle_hills",
          "modified_jungle",
          "modified_jungle_edge"
        ],
        taiga: [
          "snowy_taiga",
          "snowy_taiga_hills",
          "snowy_taiga_mountains",
          "taiga",
          "taiga_hills",
          "taiga_mountains"
        ],
        mega_taiga: [
          "giant_spruce_taiga",
          "giant_spruce_taiga_hills",
          "giant_tree_taiga",
          "giant_tree_taiga_hills"
        ],
        snowy: [
          "ice_spikes",
          "snowy_beach",
          "snowy_mountains",
          "snowy_taiga",
          "snowy_taiga_hills",
          "snowy_taiga_mountains",
          "snowy_tundra"
        ]
      }
    };
  }
};
