import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./views/Home.vue";
import Advanced from "./views/Advanced.vue";
import Statistics from "./views/Statistics.vue";
import About from "./views/About.vue";
import Seed from "./views/Seed.vue";
import Download from "./views/Download.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/advanced",
      name: "advanced",
      component: Advanced
    },
    {
      path: "/stats",
      name: "statistics",
      component: Statistics
    },
    {
      path: "/download",
      name: "download",
      component: Download
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/seed/:seed",
      name: "seed",
      component: Seed
    }
  ]
});
